// Vertical
vx=argument0
vy=argument1
repeat(abs(vy)) {
    if (!place_meeting(x, y + sign(vy), oWall ))
        y += sign(vy); 
    else {
        if instance_place(x, y + sign(vy), oWall).solid=true
        vy = 0;
        break;
    }
}

// Horizontal
repeat(abs(vx)) {
    if (!place_meeting(x + sign(vx), y, oWall))
        x += sign(vx); 
    else {
    if instance_place(x + sign(vx), y, oWall).solid=true
        vx = 0;
        break;
    }
}
