draw_set_halign(fa_left)
draw_set_colour(c_white)
var leftXBorder = view_xport[0]+16
    //MONEY/BANK
    draw_sprite(sSmallDollar,0,leftXBorder-8,view_yport[0]+8)
    draw_text(leftXBorder,view_yport[0]+8,string(money))
    //HUMAN RESOURCES
    draw_sprite(sSmallSmiley,0,leftXBorder+48,view_yport[0]+8)  
    draw_text(leftXBorder+64,view_yport[0]+8,string(HR))
    //TESTING
    if (ds_map_find_value(features,"Test level indicator")="OK"){
    draw_text(leftXBorder+96,view_yport[0]+8,"Test : "+string(testfail))
    }
    //IDEAS
    if (ds_map_find_value(features,"Idea indicator")="OK"){
       draw_sprite(sSmallIdea,0,leftXBorder+128,view_yport[0]+8)  
    draw_text(leftXBorder+144,view_yport[0]+8,string(nIdea))
    draw_line_width(leftXBorder+32+16+32,view_hport[0]-16*5.25, leftXBorder+ 64*nIdea/10,view_hport[0]-16*5.25,3)}
    //
    var displayTime
    displayTime=(time/deadline)*100
    draw_text(leftXBorder+160,view_yport[0]+8,"Deadline : "+string(displayTime)+" % ") 
    draw_line_width(leftXBorder+8,view_hport[0]-16*1.5, leftXBorder+ 64*displayTime/100,view_hport[0]-16*1.5,3)
