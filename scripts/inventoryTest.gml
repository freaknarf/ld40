///mouse wheel
if ds_exists(inventory,ds_type_list){
    if mouse_wheel_up() inventoryIndex++
    if mouse_wheel_down() inventoryIndex--
    inventoryIndex=clamp(inventoryIndex,-2,ds_list_size(inventory))
}
