////playerMove
    vsp=(d-u)*spd/spdRatio
    hsp=(r-l)*spd/spdRatio
    mask_index=s4x4
    if checkFeatured("Collide++")
        collision(hsp,vsp)
    else
        collisionBad()   
    x=clamp(x,0,room_width)
    y=clamp(y,0,room_height)
